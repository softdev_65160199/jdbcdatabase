/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



/**
 *
 * @author sarit
 */
public class SelectDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String URL = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        } 
        
        String sql = "SELECT * FROM CATEGORY";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                System.out.println(rs.getInt("category_id") + " " + rs.getString("category_name"));
            }
             
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
        if(conn!= null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
    }
}

